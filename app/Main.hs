module Main (main) where

-- import Data.Text
import Text.Printf
import System.Environment

pagewidth :: Double ; pagewidth = 1000.0
pageheight :: Double ; pageheight = 1000.0
mid_x :: Double ; mid_x = pagewidth / 2
mid_y :: Double ; mid_y = pageheight / 2

to_radians :: Double -> Double
to_radians d = (pi / 180.0) * d

preamble :: IO ()
preamble =
  putStrLn "<?xml version=\"1.0\" standalone=\"no\"?> \
 \ <!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"> \
 \ <svg width='300cm' height='300cm' version=\"1.1\" \
 \     xmlns=\"http://www.w3.org/2000/svg\"> \
 \ <desc>Radar</desc> \
 \ "

postamble :: IO ()
postamble =
  putStrLn "</svg>"

drawsegment_element :: Double ->  Double -> Double -> Double -> IO ()
drawsegment_element start_angle end_angle inner_radius outer_radius = do
  let inner_start_x = mid_x + (sin start_angle) * inner_radius
  let inner_start_y = mid_y + (cos start_angle) * inner_radius
  let inner_end_x = mid_x + (sin end_angle) * inner_radius
  let inner_end_y = mid_y + (cos end_angle) * inner_radius
  let outer_start_x = mid_x + (sin start_angle) * outer_radius
  let outer_start_y = mid_y + (cos start_angle) * outer_radius
  let outer_end_x = mid_x + (sin end_angle) * outer_radius
  let outer_end_y = mid_y + (cos end_angle) * outer_radius
  printf "<path d='M %f %f A %f %f 0 0 0 %f %f L %f %f M %f %f L %f %f A %f %f 0 0 0 %f %f ' fill='none' stroke='black' stroke-width='2' />\n"
    outer_start_x outer_start_y -- M
    outer_radius outer_radius   -- A
    outer_end_x outer_end_y
    inner_end_x inner_end_y     -- L to inner
    outer_start_x outer_start_y -- M
    inner_start_x inner_start_y     -- L
    inner_radius inner_radius     -- A
    inner_end_x inner_end_y

draw_segment :: Int -> Double -> Double -> Double -> Double -> IO ()
draw_segment ring_count ring_space ring_width s e = do
  mapM_ (\x -> drawsegment_element s e (ring_space * fromIntegral x) (ring_space * (fromIntegral x) + ring_width)
            ) [1..ring_count]

main :: IO ()
main = do
  args <- getArgs -- seg_cnt ring_cnt data_File
  let segment_count = read (args !! 0) :: Int
  let ring_count = read (args !! 1) :: Int
  let segment_angles = 340.0 / fromIntegral segment_count
  let segment_space = 360.0 / fromIntegral segment_count
  let ring_space = (pagewidth / 2) / fromIntegral (ring_count + 1)
  let ring_width = ring_space * 0.9

  let segments = [0..segment_count -1]

  preamble

  mapM_ (\x -> do
            let s = to_radians $ 10 + segment_space * fromIntegral x
            let e = s + to_radians segment_angles
            draw_segment ring_count ring_space ring_width s e
           ) segments
  -- draw_segment s e
  -- drawsegment_element s e 200.0 290.0
  postamble
